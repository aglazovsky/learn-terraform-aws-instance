terraform {
  backend "remote" {
    organization = "tf-tut"
    workspaces {
      name = "Example-Workspace"
    }
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}

resource "tls_private_key" "id_rsa" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = var.key_name
  public_key = tls_private_key.id_rsa.public_key_openssh
}

resource "local_file" "private_key" {
    content  = tls_private_key.id_rsa.private_key_pem
    filename = "ec2.pem"
    file_permission = 400
}

data "http" "ip" {
  url = "http://ipv4.icanhazip.com"
}

# default security group to access
# instances over SSH and HTTP
resource "aws_security_group" "default" {
    name = "terraform_security_group"
    description = "Used in the terraform"

    # allowed ingress from owner ip only
    # SSH access
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${chomp(data.http.ip.body)}/32"]
    }

    # HTTPS access
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["${chomp(data.http.ip.body)}/32"]
    }

    # HTTP access
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["${chomp(data.http.ip.body)}/32"]
    }

    # outbound internet access
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

data "template_cloudinit_config" "config" {
    gzip = false
    base64_encode = false

    part {
        content_type = "text/cloud-config"
        content = <<EOF
packages:
    - python3
    - docker.io
EOF
    }
}

resource "aws_instance" "server" {
  ami = "ami-08d70e59c07c61a3a"
  instance_type = "t2.micro"
  count = var.cnt
  key_name = aws_key_pair.generated_key.key_name
  security_groups = [aws_security_group.default.name]
  user_data = data.template_cloudinit_config.config.rendered

  tags = {
    Name = "Server-${count.index}"
  }

  provisioner "remote-exec" {
  inline = ["echo 'ssh is up'", "until [ -f /var/lib/cloud/instance/boot-finished ]; do sleep 1;done"]

  connection {
    host        = self.public_ip
    type        = "ssh"
    user        = var.os_user
    private_key = file(local_file.private_key.filename)
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ${var.os_user} -i '${self.public_ip},' --private-key ${local_file.private_key.filename} -e 'ansible_python_interpreter=/usr/bin/python3' run_docker_nginx.yml"
  }

}

output "inst_ip_addresses" {
  value = {
    for inst in aws_instance.server:
    inst.tags.Name => inst.public_ip
  }
}
