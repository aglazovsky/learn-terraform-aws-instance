variable "region" {
  default = "us-west-2"
}

variable "cnt" {
  default = "2"
}

variable "key_name" {}
variable "os_user" {}
